# -*- coding: utf-8 -*-
from random import randrange

action = []
SIMULATIONS_LENGTH = 5
valorRetornoCarteiraMelhor = 0
valorRetornoSimulacaoMelhor = 0
relatory = "Relatório Ações \n\n"
betterNeighborAll = ""
betterNeighborSimulation = ""
betterSimulation = "Solução Inicial"


def solutionInitialList():
    global relatory
    actions = []
    actions.append(30)
    actions.append(25)
    actions.append(20)
    actions.append(15)
    actions.append(10)

    relatory = "Solução inicial: " + str(actions)
    return actions


def generateSimulation(actionOrder, simulationNumber):
    global relatory, betterNeighborSimulation, betterSimulation, valorRetornoSimulacaoMelhor, valorRetornoCarteiraMelhor

    countNeighbor = 0
    valorRetornoSimulacaoMelhor = 0
    betterNeighborSimulation = 0

    returnCalculateY(actionOrder, countNeighbor, simulationNumber)

    countNeighbor = countNeighbor + 1

    for i in range(0, 5):
        for j in range(0, 5):

            if (i != j):
                neighborAplication = actionOrder[:]
                neighborAplication[i] = neighborAplication[i] + 5
                neighborAplication[j] = neighborAplication[j] - 5
                returnCalculateY(neighborAplication, countNeighbor, simulationNumber)
                countNeighbor += 1

    relatory += "\n Melhor vizinho desta simulação: Vizinho " + str(betterNeighborSimulation) + "  -  Retorno: " + str(valorRetornoSimulacaoMelhor) + "\n";


def returnCalculateY(aplication, neighborCount, simulationNumber):
    global valorRetornoCarteiraMelhor, valorRetornoSimulacaoMelhor, relatory, betterNeighborAll, betterNeighborSimulation, betterSimulation
    walletReturn = 0

    for i in range(0, 5):
        walletReturn += (aplication[i] * action[i]) / 100

    if (neighborCount != 0):
        relatory += "\n Vizinho " + str(neighborCount) + ": " + str(aplication) + " - Retorno Carteira: " + str(walletReturn)
    else:
        relatory += "  -  Retorno carteira: " + str(walletReturn)

    if (valorRetornoCarteiraMelhor <= walletReturn):
        valorRetornoCarteiraMelhor = walletReturn
        betterNeighborAll = neighborCount

        if (simulationNumber > 0):
            betterSimulation = "Simulação " + str(simulationNumber)

    if (valorRetornoSimulacaoMelhor <= walletReturn):
        valorRetornoSimulacaoMelhor = walletReturn
        betterNeighborSimulation = neighborCount


def sorterOrderActions():
    actionsOrder = []

    while len(actionsOrder) < 5:
        next = randrange(0, 35, 5)

        if (next >= 10):
            actionsOrder.append(next)

    return actionsOrder


def actionsInit():
    global action
    action.append(0.42292)
    action.append(0.25685)
    action.append(0.04128)
    action.append(0.05812)
    action.append(0.01731)


actionsInit()
print("Inicia a geração dos relatórios de ações: " + str(action[0]) + " | " + str(action[1]) + " | " + str(action[2]) + " | " + str(action[3]) + " | " + str(action[4]))
orderAction = solutionInitialList()
generateSimulation(orderAction, 0)
i = 0
for i in range(0, SIMULATIONS_LENGTH):
    orderAction = sorterOrderActions()
    relatory += "\n Simulação " + str(i + 1) + ": " + str(orderAction)
    generateSimulation(orderAction, (i + 1))

relatory += "\n\n Melhor vizinho entre todas as simulações: " + str(betterSimulation) + " - Vizinho " + str(betterNeighborAll) + "  -  Retorno: " + str(valorRetornoCarteiraMelhor);

print(relatory)

